var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));


/* GET users listing. */
router.get('/', function(req, res, next) {

	var headers = req.headers;
	var Sequelize = require('sequelize');
	const sequelize = new Sequelize('sakila', 'root', '', {
  		dialect: 'mysql'
	});

	var user ={}; 

	sequelize
  		.query(
    		'SELECT * FROM user WHERE email = ?',
    	{ raw: true, replacements: [headers.email] }
  		).then(function(row) {
          // Also - never store passwords in plain text
          try{
          	  bcrypt.compare(headers.password, row[0][0].password, function (err, result) {
      	  		if (result == true && row[0][0].email === headers.email) {
		               res.json({success: true, row:row[0][0]});
		          }
		          else {
		           	   res.json({success: false, incorrect: true, row:row});   
		          }
          	  	});
          }catch(e){
          	 res.json({success: false, incorrect: true, exception:e});
          }
      	});
});


module.exports = router;
