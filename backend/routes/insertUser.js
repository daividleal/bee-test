var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));


/* GET users listing. */
router.get('/', function(req, res, next) {

	var headers = req.headers;
	var Sequelize = require('sequelize');
	const sequelize = new Sequelize('sakila', 'root', '', {
  		dialect: 'mysql'
	});

	var user = {}; 

bcrypt.hash(headers.password, 10, function(err, hash) {
	sequelize
  		.query(
    		'INSERT INTO `user`(`name`, `email`, `password`) VALUES (?,?,?)',
    	{ raw: true, replacements: [headers.name, headers.email, hash] }
  		)
  		.then(function(row){
        res.send(200, {id:row[0], name:headers.name, email:headers.email, password:headers.password});
      });
      });
});


module.exports = router;
