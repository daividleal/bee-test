var express = require('express');
var router = express.Router();

/* GET Films listing. */
router.get('/', function(req, res, next) {

	var Sequelize = require('sequelize');

	const sequelize = new Sequelize('sakila', 'root', '', {
  		dialect: 'mysql'
	});
  	
  sequelize
      .query(
        'SELECT a.*, f.* FROM `actor` as a, `film_actor` as fa, `film` as f WHERE a.actor_id = fa.actor_id AND fa.film_id = f.film_id',
      { raw: true }
      ).then(function(row) {
        var result_query = row[0];

        var result = [];
        var prev_actor_id = 0;

        var counter = 0;
        var actor = null;
        for (var i = 0; i <= result_query.length; i++) {
          if(result_query[i] != null){
            const {actor_id, first_name, last_name, last_update } = result_query[i];
            const {film_id,title,description,release_year,language_id,original_language_id,rental_duration,rental_rate,length,replacement_cost,rating,special_features} = result_query[i];
            if(actor_id == prev_actor_id){
              actor["films"][counter] = {"film_id":film_id,"title":title,"description":description};
              counter++;
            }else{
              counter=0;
              prev_actor_id = actor_id;
              if(actor != null){
                result.push(actor);
              }
              actor = {"actor_id":actor_id,"first_name":first_name,"last_name":last_name,"last_update":last_update, "films":[]}
              actor["films"][counter] = {"film_id":film_id,"title":title,"description":description};
            }
          }
          
        }

        try{
          return res.json({success: true, incorrect: false, allActors:result}); 
        }catch(e){
          return res.json({success: false, incorrect: true, exception:e});
      }
    }
  );
});

module.exports = router;


//2318 ,release_year,language_id,original_language_id,rental_duration,rental_rate,length,replacement_cost,rating,special_features