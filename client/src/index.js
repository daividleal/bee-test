import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './Login.css';
import Login from './Login';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<Login/>, document.getElementById('body'));
registerServiceWorker();