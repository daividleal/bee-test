import React from 'react';
import PropTypes from 'prop-types';
require("bootstrap/less/bootstrap.less");

const ActorCard = props => {
  const { first_name = null, actor_id = 0, last_name = null, last_update = null ,films = [] } = props.actor || {};

  return (
    <div className="container">
      <div className="panel-group">
      <div className="panel panel-default">
        <div className="panel-heading">
          <h4 className="panel-title">
            <a data-toggle="collapse" href={"#coll_"+actor_id}>{first_name}</a>
          </h4>
        </div>
        <div id={"coll_"+actor_id} className="panel-collapse collapse">
          <div className="panel-body">
            <p>{first_name} {last_name}</p>
              {films.map(film => {if(film != null){return <span><p></p>{film.title} - {film.description}</span>}})}
          </div>
          <div className="panel-footer">Last Update: {last_update}</div>
        </div>
      </div>
    </div>

       
    </div>
  )
}

ActorCard.propTypes = {
  actor: PropTypes.shape({
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    last_update: PropTypes.string.isRequired
  }).isRequired
};

export default ActorCard;