import React, { Component } from "react";
import ReactDOM from 'react-dom';
import Pagination from './components/Pagination';
import ActorsCard from './components/ActorsCard';
import { Button } from "react-bootstrap";
import Login from './Login'
require("bootstrap/less/bootstrap.less");


export default class Dashboard extends Component{

  componentDidMount() {
      fetch("/listActors")
        .then(response => response.json())
      .then(data => {
        this.setState(data);
        console.log(data);
        console.log(this.state);
      });
  }
  
  state = { allActors: [], currentActors: [], currentPage: null, totalPages: null }

  onPageChanged = data => {
    const { allActors } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentActors = allActors.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentActors, totalPages });
  }

  logOut(){
    alert("Sorry to hear that, By!");
    ReactDOM.render(<Login/>, document.getElementById('body'));
  }

  handleSubmit = event => {
    event.preventDefault();
    this.logOut();
  }

  render() {
    const { allActors, currentActors, currentPage, totalPages } = this.state;
    const totalActors = allActors.length;

    if (totalActors === 0) return null;

    const headerClass = ['text-dark py-2 pr-4 m-0', currentPage ? 'border-gray border-right' : ''].join(' ').trim();

    return (
      <div className="container mb-5">
        <div className="row d-flex flex-row py-5">

          <div className="w-100 px-4 py-5 d-flex flex-row flex-wrap align-items-center justify-content-between">
            <div className="d-flex flex-row align-items-center">

              <h2 className={headerClass}>
                <strong className="text-secondary">{totalActors}</strong> Actors
              </h2>

              { currentPage && (
                <span className="current-page d-inline-block h-100 pl-4 text-secondary">
                  Page <span className="font-weight-bold">{ currentPage }</span> / <span className="font-weight-bold">{ totalPages }</span>
                </span>
              ) }
              <p></p>
              <form onSubmit={this.handleSubmit}>
                  <Button type="submit">Log Out</Button>
              </form>                  

            </div>

            <br></br>
            <div className="d-flex flex-row py-4 align-items-center">
              <Pagination totalRecords={totalActors} pageLimit={this.state.pageLimit} pageNeighbours={1} onPageChanged={this.onPageChanged} />
            </div>
            <br></br>
          </div>
          
          {currentActors.map(actor => { if(actor != null){ return <ActorsCard key={actor.actor_id} actor={actor}/>;}})}

        </div>
      </div>
    );
  }

}


