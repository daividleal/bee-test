import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import SignUp from "./SignUp.js";
import ReactDOM from 'react-dom';
import Dashboard from './Dashboard';

export default class Login extends Component {

  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      email: "",
      password: ""
    };
    
  }

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  login(email){ 
      fetch("/users", {
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            "email": this.state.email,
            "password": this.state.password
        }
    }).then(function(response) {
    return response.json();
  })
  .then(function(data) {
    if(data.success){
        alert("Wellcome to our Dashboard ", data.name);
        ReactDOM.render(<Dashboard/>, document.getElementById('body'));
    }else{
        alert('Permission Denied');
    }
  }).catch(function(error) {
        alert("Sorry, try again latter! " + error);
    });
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
    event.preventDefault();
    this.login();
  }

  goToSignUpForm(){
    ReactDOM.render(<SignUp/>, document.getElementById('body'));
  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
            <Button
              block
              bsSize="large"
              disabled={!this.validateForm()}
              type="submit">Login</Button>
            <Button
              block
              className="btn-primary"
              bsSize="large"
              onClick={this.goToSignUpForm}>SignUp</Button>
        </form>
      </div>
    );
  }
}
