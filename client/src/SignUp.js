import React, { Component } from "react";
import { Button } from "react-bootstrap";
import "./Login.css";
import ReactDOM from 'react-dom';
import Dashboard from './Dashboard';
import Login from './Login';
import "./style.css";
/*import bcrypt from 'bcryptjs';*/

export default class SignUp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      confirm: "",
      name: ""
    };
  }

  handleSubmit = event => {
    event.preventDefault();
    this.register();
  }

  register(){

    /*var name = this.state.name;
    var email = this.state.email;
    bcrypt.hash(this.state.password, 10, function(err, hash) {*/
    fetch("/insertUser", {
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            "name": this.state.name,
            "email": this.state.email,
            "password": this.state.password,
            // "Content-Type": "application/x-www-form-urlencoded",
        }
    })
    .then(response => response.json())
    .then(function(data){
      alert("Congrats, you are now a new member!");
      ReactDOM.render(<Login/>,  document.getElementById('body'));
    }).catch(function(error) {
        alert("Sorry, try again latter!");// If there is any error you will catch them here
    });

   /* });*/
  }

  validateForm() {
    console.log(this.state.name);
    return this.state.email.length > 0 
            && this.state.password.length > 0 
            && this.state.name.length > 0 
            && this.state.confirm.length > 0
            && (this.state.confirm === this.state.password);
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  render() {
    return (
        <div className="container">
      <div className="row main">
        <div className="panel-heading">
                 <div className="panel-title text-center">
                    <h1 className="title">Register</h1>
                    <hr />
                  </div>
              </div> 
        <div className="main-login main-center">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="name" className="cols-sm-2 control-label">Your Name</label>
              <div className="cols-sm-10">
                <div className="input-group">
                  <span className="input-group-addon"><i className="fa fa-user fa" aria-hidden="true"></i></span>
                  <input type="text" value={this.state.name} onChange={this.handleChange} className="form-control" name="name" id="name"  placeholder="Enter your Name"/>
                </div>
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="email" className="cols-sm-2 control-label">Your Email</label>
              <div className="cols-sm-10">
                <div className="input-group">
                  <span className="input-group-addon"><i className="fa fa-envelope fa" aria-hidden="true"></i></span>
                  <input type="text" value={this.state.email} onChange={this.handleChange} className="form-control" name="email" id="email"  placeholder="Enter your Email"/>
                </div>
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="password" className="cols-sm-2 control-label">Password</label>
              <div className="cols-sm-10">
                <div className="input-group">
                  <span className="input-group-addon"><i className="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input type="password" value={this.state.password} onChange={this.handleChange} className="form-control" name="password" id="password"  placeholder="Enter your Password"/>
                </div>
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="confirm" className="cols-sm-2 control-label">Confirm Password</label>
              <div className="cols-sm-10">
                <div className="input-group">
                  <span className="input-group-addon"><i className="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input value={this.state.confirm} onChange={this.handleChange} type="password" className="form-control" name="confirm" id="confirm"  placeholder="Confirm your Password"/>
                </div>
              </div>
            </div>

            <div className="form-group ">
              <Button type="submit" disabled={!this.validateForm()} className="btn-primary">Register</Button>
            </div>
            <div className="login-register">
                    <a href="index">Login</a>
                 </div>
          </form>
        </div>
      </div>
    </div>
    );
  }
}
